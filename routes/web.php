<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index');
Route::get('/login', 'LoginController@index');

Route::get('/logout',function(){
    session(['userToken' => '']);
    session(['userData' => '']);
    session()->flush();
    return redirect('/login');
});

/*
Route::get('/{any}', function () {
    return view('layouts/app');
})->where('any', '.*'); 
*/



Route::post('/proses-login', 'LoginController@prosesLogin');


Route::group(['middleware' => ['CheckAuth']], function () {
    
    Route::get('/beranda', 'BerandaController@index');    
    
    Route::get('/pengajuan-tenant', 'PermintaanController@tenant');
    Route::get('/pengajuan-tenant-table/{from?}/{to?}/{status?}', 'PermintaanController@tenantTable');
    
    Route::get('/pengajuan-internal', 'PermintaanController@internal');
    Route::get('/pengajuan-internal-table/{from?}/{to?}/{status?}', 'PermintaanController@internalTable');

    Route::get('/lk3', 'Lk3Controller@index');    
    Route::get('/surat-izin', 'SuratIzinController@index');    

    Route::get('/master-data-building', 'MasterDataController@building');    
    Route::get('/master-data-unit', 'MasterDataController@unit');    
    Route::get('/master-data-service', 'MasterDataController@service');    
    Route::get('/master-data-category-service', 'MasterDataController@categoryService');    
    Route::get('/master-data-tenant', 'MasterDataController@tenant');    
    Route::get('/master-data-business-type', 'MasterDataController@businessType');    
    Route::get('/master-data-business-group', 'MasterDataController@busionessGroup');    
    Route::get('/master-data-staff', 'MasterDataController@staff');    
    Route::get('/master-data-position', 'MasterDataController@position');    
    Route::get('/master-data-market-place', 'MasterDataController@marketPlace');    
    Route::get('/master-data-type-markert-place', 'MasterDataController@typeMarketPlace');    
    

    Route::get('/banner', 'BerandaController@banner');


});





Route::get('test-oke',function() {
    echo phpinfo();
});