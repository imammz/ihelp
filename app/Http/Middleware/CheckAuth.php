<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\GeneralController;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sess = session()->all();

        if(!isset($sess['userData']->userinfo->username)) {
            return redirect('/login')->with('error','Sesi Login Habis Atau Sedang Login Dengan Perangkat Lain');
        }

        $get = new GeneralCOntroller();
        $cek = $get->getData('bannerpromotions');
        $cek = (array) $cek;

        if(isset($cek['error'])) { 
             return redirect('/login')->with('error','Sesi Login Habis Atau Sedang Login Dengan Perangkat Lain');
        }

        return $next($request);
    }
}