<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BerandaController extends GeneralController
{
    //
    protected $header = [];
 
    public function __construct()
    {

    }


    public function index() {
        
        $userData = session('userData');
        
        $data = [];
        $data['title'] = "Beranda | iHelp";
        $data['userData'] = $userData;
        $data['current'] = 'BERANDA';
        $data['menus'] = $this->_getMenu();    
        $data['aturan'] = $this->getData('aturans');
        $data['newsletters'] = $this->getData('newsletters');
        $data['reminders'] = $this->getData('reminders');

        $data['tenant_Open'] = $this->getData('t_transaksi?filter_status=1&transaksi_typeid=1');
        $data['tenant_Progress'] = $this->getData('t_transaksi?filter_status=3&transaksi_typeid=1');
        $data['tenant_Done'] = $this->getData('t_transaksi?filter_status=5&transaksi_typeid=1');
        $data['tenant_Pending'] = $this->getData('t_transaksi?filter_status=7&transaksi_typeid=1');
        
    


        $data['internal_Open'] = $this->getData('t_transaksi?filter_status=1&transaksi_typeid=2');
        $data['internal_Progress'] = $this->getData('t_transaksi?filter_status=3&transaksi_typeid=2');
        $data['internal_Done'] = $this->getData('t_transaksi?filter_status=5&transaksi_typeid=2');
        $data['internal_Pending'] = $this->getData('t_transaksi?filter_status=7&transaksi_typeid=2');
        
     

        

        return view('beranda',$data);
       
    }

    public function banner() {
        
        $data = [];
        $data['banner'] = $this->getData('bannerpromotions');
 

        return view('slider',$data);
    }

}