<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class serviceController extends GeneralController
{
    //
    protected $header = [];
 
    public function __construct()
    {

    }
    
    public function permissionRequest() {

        $data = [];
        $data['userData'] = session('userData');

        return view('permission-request',$data);
    }

    public function addPermissionRequest() {
        $data = [];
        $data['userData'] = session('userData');

        return view('add-permission-request',$data);

    }

    public function tenantComplain() {

        $data = [];
        $data['userData'] = session('userData');

        return view('permission-request',$data);
    }

    public function addTenantComplain() {
        $data = [];
        $data['userData'] = session('userData');

        return view('add-permission-request',$data);

    }
}
