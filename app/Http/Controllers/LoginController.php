<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class LoginController extends GeneralController
{
    //

    public function index() {

        return view('login');
    }

    public function prosesLogin(Request $request) {
        
        $send = [];
        $send['email'] = $request->email;
        $send['Password'] = $request->password;
        $validation = 1;

        if($send['email'] == '' OR $send['Password'] == '') {   
            $validation = 0;
        }

        $ress = $this->postDataUnauth('login',$send);


        if(isset($ress->error) or $validation == 0) {
            return redirect('/')
            ->with('error','Login Gagal - Periksa Kembali Email atau Password Anda');
         }
         else {


        if($ress->status) {

            $menu = [];
            $menu[] = [
                ['name'=>'Beranda','link'=>'','submenu'=>[]],
            
            ] ;

            session(['userData'=>$ress->data]);
          
            return redirect('/beranda');
            
        }
        else {


        }   


         }

      
        
       
    }

}