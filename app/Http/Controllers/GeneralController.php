<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Provider\Uuid;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
  
    public function __construct()
    {
  
       
    }


    public function getUserData() {
        return session('userData');
    }

    public function getHeader() {


    $userData = $this->getUserData();


        return ['Content-Type' => 'multipart/form-data',
                'Authorization' =>'Bearer '.$userData->userinfo->token 
                ];
  
    }




    public function getData($url) {

        try {
            $header = $this->getHeader();
            $client = new Client();
            $req = $client->request(
                'GET',
                env('API_URL') . $url,
                [
        'headers' => $header]
            );

            $ress = json_decode($req->getBody()->getContents());

            return $ress;
        }

        catch (\Exception $e) {
            return ['error'=>$e->getMessage()];
        }


    }


    public function postData($url,$data,$auth = true) {

        try {
            $header = $this->getHeader();
            $client = new Client();
            $req = $client->request(
                'POST',
                env('API_URL') . $url,
                [
                    'form_params' => $data,
                    'headers' => $header
                ]
            );

            $ress = json_decode($req->getBody()->getContents());

            return $ress;
        }

        catch (\Exception $e) {
            return (object) ['error'=>$e->getMessage()];
        }


    }


      public function postDataUnauth($url,$data,$auth = true) {

        try {
            $client = new Client();
            $req = $client->request(
                'POST',
                env('API_URL') . $url,
                [
                    'form_params' => $data
                ]
            );

            $ress = json_decode($req->getBody()->getContents());

            return $ress;
        }

        catch (\Exception $e) {
            return (object) ['error'=>$e->getMessage()];
        }


    }

    public function putData($url,$data) {

        try {
            $header = $this->getHeader();
            $client = new Client();
            $req = $client->request(
                'PUT',
                env('API_URL') . $url,
                [
            'form_params' => $data,
        'headers' => $header]
            );

            $ress = json_decode($req->getBody()->getContents());

            return $ress;
        }

        catch (\Exception $e) {
            return ['error'=>$e->getMessage()];
        }


    }


    public function deleteData($url) {

        try {
            $header = $this->getHeader();
            $client = new Client();
            $req = $client->request(
                'DELETE',
                env('API_URL') . $url,
                [
        'headers' => $header]
            );

            $ress = json_decode($req->getBody()->getContents());

            return $ress;
        }

        catch (\Exception $e) {
            return ['error'=>$e->getMessage()];
        }


    }


    public function _getMenu() {
        $data = [];
        $data[] = ['menu'=>'Beranda','link'=>'beranda','icon'=>'fas fa-home','submenu'=>[]];
        $data[] = ['menu'=>'Permintaan/Keluhan','link'=>'javascript:;','icon'=>'far fa-edit',
                'submenu'=>[
                       ['menu'=>'Pengajuan Tenant','link'=>'pengajuan-tenant','icon'=>'far fa-list-alt'],
                       ['menu'=>'Pengajuan Internal','link'=>'pengajuan-internal','icon'=>'far fa-file-alt']
                ]
            ];
        $data[] = ['menu'=>'Surat Izin','link'=>'surat-izin','icon'=>'far fa-envelope',
                'submenu'=>[]
            ];
        $data[] = ['menu'=>'LK3','link'=>'lk3','icon'=>'far fa-window-restore',
                'submenu'=>[]
            ];
        $data[] = ['menu'=>'Master Data','link'=>'','icon'=>'fas fa-hdd',
                 'submenu'=> [
                      ['menu'=>'Building','link'=>'master-data-building','icon'=>''],
                      ['menu'=>'Unit','link'=>'master-data-unit','icon'=>''],
                      ['menu'=>'Category Service','link'=>'master-data-category-service','icon'=>''],
                      ['menu'=>'Type Category Service','link'=>'master-data-type-category-service','icon'=>''],
                      ['menu'=>'Tenant','link'=>'master-data-tenant','icon'=>''],
                      ['menu'=>'Business Type','link'=>'master-data-business-type','icon'=>''],
                      ['menu'=>'Business Group','link'=>'master-data-business-group','icon'=>''],
                      ['menu'=>'Staff','link'=>'master-data-staff','icon'=>''],
                      ['menu'=>'Position','link'=>'master-data-position','icon'=>''],
                      ['menu'=>'Market Place','link'=>'master-data-market-place','icon'=>''],
                      ['menu'=>'Type Market Place','link'=>'master-data-type-markert-place','icon'=>''],
                 ]
                     ];
 
        return $data;
    }


}