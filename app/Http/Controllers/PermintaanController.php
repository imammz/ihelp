<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PermintaanController extends GeneralController
{
    //

    
    public function tenant() {
        
        $userData = session('userData');
        
        $data = [];
        $data['title'] = "Permintaan/Keluhan Tenant | iHelp";
        $data['userData'] = $userData;
        $data['current'] = 'PERMINTAAN/KELUHAN';
        $data['menus'] = $this->_getMenu();   
        $data['service_status'] = $this->getData('service_statuss'); 



        return view('pengajuan-tenant',$data);
       
    }
    public function tenantTable($from = null, $to = null, $status = 1) {
        
        $userData = session('userData');
        
        $data = [];
        $data['userData'] = $userData;
         $data['permintaan'] = $this->getData('t_transaksi?filter_status='.$status.'&transaksi_typeid=1');
   
        return view('table-pengajuan-tenant',$data);
    }



     public function internal() {
        
        $userData = session('userData');
        
        $data = [];
        $data['title'] = "Permintaan/Keluhan Tenant | iHelp";
        $data['userData'] = $userData;
        $data['current'] = 'PERMINTAAN/KELUHAN';
        $data['menus'] = $this->_getMenu();   
        $data['service_status'] = $this->getData('service_statuss'); 



        return view('pengajuan-internal',$data);
       
    }
    public function internalTable($from = null, $to = null, $status = 1) {
        
        $userData = session('userData');
        
        $data = [];
        $data['userData'] = $userData;
         $data['permintaan'] = $this->getData('t_transaksi?filter_status='.$status.'&transaksi_typeid=2');
   
        return view('table-pengajuan-internal',$data);
    }

}