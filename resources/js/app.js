window.Vue = require('vue').default;

import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './components/App.vue';
import VueSweetalert2 from 'vue-sweetalert2';
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);

import Beranda from './components/Beranda.vue';
import SuratJalan from './components/SuratJalan.vue';

const routes = [{
        name: 'beranda',
        path: '/',
        component: Beranda
    },
    {
        name: 'suratJalan',
        path: '/surat-jalan',
        component: SuratJalan
    },
];

const router = new VueRouter({ mode: 'history', routes: routes });
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');