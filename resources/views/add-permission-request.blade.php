@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/pages/app-calendar.min.css">
@endsection


@section('content')

<!--Fixed Width Tabs-->
<div class="row"> 
    <div class="col s12 m12 l12"> 
        <div  class="card card-default">
            <div class="card-content">
                
                <h3 class="card-title animate fadeUp">
                    <i class="material-icons prefix">add_box</i>
                    Add Permision Request</h3>
            </div>
        </div>
    </div>
</div>

<div class="row"> 
    <div class="col s6 m6 l6"> 
        <div  class="card card-default">
            <div class="card-content">
                <div class="input-field">  
                    <i class="material-icons prefix">assignment</i>
                    <select>
                        <option value="" disabled selected>Select Category</option>
                        <option value="1">Surat Ijin Kegiatan</option>
                        <option value="2">Surat Ijin Penyebaran Brosur</option>
                        <option value="3">Surat Ijin Pemasangan Long Banner</option>
                        <option value="4">Surat Ijin Tutup Toko</option>
                        <option value="4">Surat Ijin Masuk Barang</option>
                        <option value="4">Surat Ijin Kerja</option>
                        <option value="4">Surat Ijin Keluar Barang</option>
                        <option value="4">Surat Ijin Overtime</option>
                      </select>
                      <label>Category</label>
                </div>

                <div class="input-field">  
                    <i class="material-icons prefix">date_range</i>
                    <input type="text" class="datepicker" id="from">
                            <label for="from">Start Date</label>
                </div>
                <div class="input-field">  
                    <i class="material-icons prefix">timer</i>
                    <input type="text" class="timepicker" id="timeFrom">
                            <label for="timeFrom">Time Start</label>
                </div>



            </div>
        </div>

    </div>

    <div class="col s6 m6 l6"> 
        <div  class="card card-default">
            <div class="card-content">
                <div class="input-field">  
                    <hr/>
                </div>
            
                            <div class="input-field">  
                                <i class="material-icons prefix">date_range</i>
                                <input type="text" class="datepicker" id="from">
                                        <label for="from">End Date</label>
                            </div>
                            <div class="input-field">  
                                <i class="material-icons prefix">timer</i>
                                <input type="text" class="timepicker" id="timeFrom">
                                        <label for="timeFrom">Time End</label>
                            </div>
            
            
            
                        
            </div>
        </div>
    </div>



 </div>

 <div class="row"> 
    <div class="col s12 m12 l12"> 
        <div  class="card card-default">
            <div class="card-content">
                <h3 class="card-title animate fadeUp">
                    <i class="material-icons prefix">book</i>
                    Permision Information</h3>
            </div>
        </div>
    </div>
</div>

<div class="row"> 
    <div class="col s12 m12 l12"> 
        <div  class="card card-default">
            <div class="card-content">
                <div class="input-field">
                    <i class="material-icons prefix">border_color</i>
                    <input id="last_name" type="text" class="validate">
                    <label for="last_name">Title</label>
                  </div>
            </div>
        </div>
    </div>
</div>
<div class="row"> 
    <div class="col s6 m6 l6"> 
        <div  class="card card-default">
            <div class="card-content">
                <div class="input-field">
                    <i class="material-icons prefix">account_circle</i>
                    <input id="tenant" type="text" class="validate">
                    <label for="tenant">Tenant PIC</label>
                  </div>
            </div>
        </div>
    </div>
    <div class="col s6 m6 l6"> 
        <div  class="card card-default">
            <div class="card-content">
                <div class="input-field">
                    <i class="material-icons prefix">phone</i>
                    <input id="hp" type="text" class="validate">
                    <label for="hp">HP PIC</label>
                  </div>
            </div>
        </div>
    </div>
</div>
<div class="row"> 
    <div class="col s12 m12 l12"> 
        <div  class="card card-default">
            <div class="card-content">
                <div class="input-field">
                    <i class="material-icons prefix">event_note</i>
                    <input id="note" type="text" class="validate">
                    <label for="note">Note</label>
                  </div>
            </div>
        </div>
    </div>
</div>

<div class="row"> 
    <div class="col s12 m12 l12"> 
        <div  class="card card-default text-center">
            <div class="card-content">
                <a href="{{ url('/permission-request') }}" class="btn waves-effect waves-light amber" > Cancel
                  </a> 
                <a href="{{ url('/add-permission-request') }}" class="btn waves-effect waves-light cyan" > Simpan
                  </a> 
               
            </div>
        </div>
    </div>
</div>
 


@endsection


@section('js')

<script>
   
              $('.carousel.carousel-slider').carousel({
                fullWidth: true,
                indicators: true
              });




</script>
<script src="{{url('/')}}/app-assets/js/scripts/app-calendar.min.js"></script>

@endsection