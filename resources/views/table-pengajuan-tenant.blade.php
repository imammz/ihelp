<!--begin: Datatable-->
<div style="padding: 18px 2px 18px 2px;">
    <table class="table table-bordered table-hover table-checkable" id="kt_datatable"
        style="margin-top: 13px !important;">
        <thead>
            <tr>
                <th>No</th>
                <th>Request No</th>
                <th>Category</th>
                <th>Request By</th>
                <th>Judul</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Catatan</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($permintaan->data))

            @php
            $no = 1;
            @endphp
            @foreach ($permintaan->data as $row)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $row->reqno }}</td>
                <td>{{ $row->category }}</td>
                <td>{{ $row->created_by }}</td>
                <td>{{ $row->title }}</td>
                <td>
                    <span class="text-muted">
                        ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->diffForHumans()  }})
                    </span>
                    <br />
                    <span class="text-dark">
                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->created_at))->format('d, M Y')  }}
                    </span>

                </td>
                <td>
                    @switch($row->status)

                    @case('Open')
                    @php
                    $label = 'label-info'
                    @endphp
                    @break

                    @case('Assigned')
                    @php
                    $label = 'label-warning'
                    @endphp
                    @break

                    @case('Progress')
                    @php
                    $label = 'label-primary'
                    @endphp
                    @break

                    @case('Cancel')
                    @php
                    $label = 'label-danger'
                    @endphp
                    @break

                    @case('Done')
                    @php
                    $label = 'label-success'
                    @endphp
                    @break

                    @case('Verified')
                    @php
                    $label = 'label-dark'
                    @endphp
                    @break

                    @case('Pending')
                    @php
                    $label = 'label-outline-danger'
                    @endphp
                    @break



                    @default
                    @php
                    $label = 'label-primary'
                    @endphp

                    @endswitch

                    <span class='label label-xl label-info label-inline mr-2 {{ $label }}'> {{ $row->status }} </span>
                </td>
                <td>{{ $row->desc }}</td>
                <td>
                    <a href='#' class='btn btn-icon btn-primary  btn-lg mr-4'>
                        <i class="flaticon2-sms"></i>
                    </a>
                    &nbsp;
                    <a href='#' class='btn btn-icon btn-warning  btn-lg mr-4'>
                        <i class="far fa-calendar-check"></i>
                    </a>
                </td>
            </tr>
            @php
            $no++;
            @endphp
            @endforeach

            @endif

        </tbody>
        <tfoot>
            <tr>
                <th>No</th>
                <th>Request No</th>
                <th>Category</th>
                <th>Request By</th>
                <th>Judul</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Catatan</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
</div>

<!--end: Datatable-->

{{--  
<script src="{{ url('/') }}/themes/v8/assets/js/pages/crud/datatables/basic/basic.js"></script> --}}

<script src="{{ url('/') }}/themes/v8/assets/js/pages/crud/datatables/extensions/keytable.js"></script>