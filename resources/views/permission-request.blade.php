@extends('layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/app-assets/css/pages/app-calendar.min.css">
@endsection


@section('content')

<!--Fixed Width Tabs-->

<div class="row"> 
    <div class="col s9 m9 l9"> 

    </div>
    <div class="col s3 m3 l3"> 
        <div>
            <a href="{{ url('/add-permission-request') }}" class="btn waves-effect waves-light" > Add Permission Request
                <i class="material-icons right">add</i>
              </a> 
        </div>

    </div>
 </div>
</div>

<div class="row">  
    <div class="col s3 m3 l3 input-field">  
        <input type="text" class="datepicker" id="from">
                <label for="from">From</label>
    </div>
    <div class="col s3 m3 l3 input-field"> 
        <input type="text" class="datepicker" id="until">
        <label for="until">Until</label>    
    </div>
    <div class="col s3 m3 l3 input-field">  
        <select>
            <option value="" disabled selected>Select Status</option>
            <option value="1">Open</option>
            <option value="2">On Progress</option>
            <option value="3">Approve</option>
            <option value="4">Disapprove</option>
          </select>
          <label>Status</label>
    </div>
    <div class="col s3 m3 l3">  </div>

</div>
<div class="row"> 
   <div class="col s12 m12 l12"> 
      <div  class="card card-default scrollspy animate fadeLeft">
         <div class="card-content">
            <h4 class="card-title animate fadeUp">Permision Request</h4>
            <table class="Highlight">
                <thead>
                    <tr>
                        <th>Unit Code </th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Tenant</th>
                        <th>Title</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Created Time</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                   

                </tbody>
            </table>
        </div>
      </div>
   </div>
</div>


@endsection


@section('js')

<script>
   
              $('.carousel.carousel-slider').carousel({
                fullWidth: true,
                indicators: true
              });




</script>
<script src="{{url('/')}}/app-assets/js/scripts/app-calendar.min.js"></script>

@endsection