<script>
    $(document).ready(function() {
        $("#loader").fadeOut(20);
        $("#page").fadeIn(500);
    });
</script>

<script>
    function cekCapil() {

        var proses = true;
        var nik = $('#NIK_PENGURUS').val();
        var nama = $('#NAMA_PENGURUS').val();
        var url = 'https://dev.siks.kemsos.go.id/api/datapbi/cekNIKSimil/' + nik + '/' + nama + '/5iKsNG-kTPeLv';

        if (nik == '') {
            alert('NIK TIDAK BOLEH KOSONG');
            proses = false;
            $('#NIK_PENGURUS').focus();
        }

        if (nama == '') {
            alert('NAMA TIDAK BOLEH KOSONG');
            proses = false;
            $('#NAMA_PENGURUS').focus();
        }

        if (proses) {

            $.get(url,
                function(data, status) {

                    alert(data.msg);

                    if (data.flag_nik == 1) {
                        $('#suksesCekNIK').show();
                        $('#btnSuksesCekNIK').show();
                        $('#gagalCekNIK').hide();
                        $('#btnGagalCekNIK').hide();
                    } else {
                        $('#suksesCekNIK').hide();
                        $('#btnSuksesCekNIK').hide();
                        $('#gagalCekNIK').show();
                        $('#btnGagalCekNIK').show();
                    }


                });

        }

    }


    $("#btnSuksesCekNIK").click(function() {

        $.post("{{ base_url() }}dinsoskab/bsp/proses_perbaikan", {
                IDBDT: $('#IDBDT').val(),
                IDARTBDT: $('#IDARTBDT').val(),
                ID_PENGURUS: $('#ID_pengurus').val(),
                NAMA_PENGURUS: $('#NAMA_PENGURUS').val(),
                NIK_PENGURUS: $('#NIK_PENGURUS').val(),
            },
            function(data, status) {
                alert(data.msg);
                goto('dinsoskab/bsp/gagal_burekol');
            });

    });



    $('#NIK_PENGURUS').keypress(function() {
        var maxLength = $(this).val().length;
        if (maxLength >= 16) {
            alert("NIK Maksimum 16 digit");
            return false;
        }
    });
</script>


<script>
    $('table tbody tr').each(function(i) {
        if (i%2 == 0) {
            $(this).children('td').css('background-color', '#EEE');
        }
    });
</script>

<script>
    $(".qty1").on("change", function () {
        var total = <?php echo $verval['kuota'] ?>;
        var sum = 0;
        $(".qty1").each(function () {
            if ($(this).val() != "")
                sum += parseInt($(this).val());
        });


        $("#totalKuota").html(formatNumber(sum));

        var hasil = total - sum;
        var hasil2 = sum - total;
        if (hasil < 0) {
            var msg = " Maaf, Melebihi Kuota Kabupaten Sebanyak  : " + hasil2;

            document.getElementById("btntotalKuota").disabled = true;
        }
        if (hasil > 0) {
            var msg = " Sisa Kuota :  " + hasil;
            document.getElementById("btntotalKuota").disabled = false;
        }
        if (hasil === 0) {
            var msg = " Kuota Sesuai ";
            document.getElementById("btntotalKuota").disabled = false;
        }
        $("#msgtotalKuota").html(msg);

    });

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
    }

    function checkSelisih(val, after, id, usulan) {

        if (val < after) {
            alert('Kuota Baru Desa Ini Tidak Boleh Kurang Dari ' + after);
            document.getElementById(id).value = after;
        }

        if (usulan == undefined) {
            usulan = 0;
        }

        var newkuota = after + usulan;

        if (val > newkuota) {
            alert('Kuota Baru Desa Ini Tidak Boleh Lebih Dari ' + newkuota + '.\n Karena Data Usulan Desa Ini Tidak Mencukupi');
            document.getElementById(id).value = newkuota;
        }
    }

    $(document).ready(function () {


            //---------------------
            var total = <?php echo $verval['kuota'] ?>;
            var sum = 0;
            $(".qty1").each(function () {
                if ($(this).val() != "")
                    sum += parseInt($(this).val());
            });

            var hasil = total - sum;
            var hasil2 = sum - total;
            if (hasil < 0) {
                var msg = " Maaf, Melebihi Kuota Kabupaten Sebanyak  : " + hasil2;

                document.getElementById("btntotalKuota").disabled = true;
            }
            if (hasil > 0) {
                var msg = " Sisa Kuota :  " + hasil;
                document.getElementById("btntotalKuota").disabled = false;
            }
            if (hasil === 0) {
                var msg = " Kuota Sesuai ";
                document.getElementById("btntotalKuota").disabled = false;
            }
            $("#msgtotalKuota").html(msg);
            $("#totalKuota").html(formatNumber(sum));
            //---------------------


        $('#adj_kuota').dataTable({
            "searching": true,
            "serverSide": false,
            "createdRow": function (row, data, index) {
                $('td', row).eq(7).css({'background-color': '#cacfca', 'text-align': 'center', 'vertical-align': 'middle'});
            },
            "lengthMenu": [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            language: {
                searchPlaceholder: "Nama Desa ... "
            },
            "pageLength": 200
        });


        $('input:text').keypress(function (e) {
            var code = e.keyCode || e.which;

            if (code === 13) {
                e.preventDefault();
                //$(".btn-submit").click();
            }
            ;
        });

        $('input:number').keypress(function (e) {
            var code = e.keyCode || e.which;

            if (code === 13) {
                e.preventDefault();
                //$(".btn-submit").click();
            }
            ;
        });

    });


    function handle(e) {
        if (e.keyCode === 13) {
            e.preventDefault(); // Ensure it is only this code that rusn

            //alert("Enter was pressed was presses");
        }
    }


</script>


<script>
    $("#mainContent").load("{{ base_url() }}dinsoskab/bsp/{{$view}}/{{$param}}"); 
    // default view


</script>

<script>
    function goto(url) {

$.get("<?php echo  base_url() ?>kemsos/login/ceksess", function(data, status){


	if(data.login) {
		$("#page").fadeOut(20);
		$("#loader").fadeIn(20);
		$("#mainContent").load("<?php echo base_url() ?>"+url);


	}
	else {
		showSessionLogin();

		setTimeout(function(){
		    $(location).attr('href','<?php echo base_url() ?>');
		},1000);
	}


});


}
showSessionLogin = function() {
alert('SESI LOGIN HABIS, HARAP LOGIN KEMBALI');
};

</script>

<script type="text/javascript">
    $(document).ready(function() {
    
        $('#belum').dataTable({
            "searching": true,
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url(); ?>dinsoskab/bsp/ajax_gagalBurekol_belum",
            "columns": [
                { "data": "counter" },
                { "data": "NMKEC" },
                { "data": "NMDESA" },
                { "data": "IDBDT" },
                { "data": "IDARTBDT" },
                { "data": "ID_PENGURUS" },
                { "data": "NIK_PENGURUS" },
                { "data": "NAMA_PENGURUS" },
                { "data": "STATUS_PKH" },
                { "data": "BANK" },
                { "data": "Ket_Proses" },
                { "data": "aksi" },
            ],
            "createdRow": function ( row, data, index ) {
                $('td', row).eq(9).css({'background-color':'#cacfca', 'text-align':'center', 'vertical-align': 'middle'});
                  $('td', row).eq(10).css({'background-color':'#facfca', 'text-align':'left', 'vertical-align': 'middle'});
            },
            "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
            language: {
                searchPlaceholder: "Nama Kec., Nama Desa, Nama KRT"
            }
        });
    
    
    
      
    });



    $('#p').removeClass('highlight strike');
    $("p:first").addClass("intro");


    function goto(url) {

        $.get("{{url('cek-sess/')}}", function(data, status){

            if(data.login) {
                $("#page").fadeOut(20);
                $("#loader").fadeIn(20);
                $("#content").load("{{url('')}}/"+url);
            }
            else {
                showSessionLogin();

                setTimeout(function(){
                    $(location).attr('href','{{url("login")}}');
                },2000);
            }


        });


    }

</script>