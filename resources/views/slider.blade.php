<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<style>
    .avatar-box {
        background-color: rgb(255, 255, 255);
        /* Fallback color */
        background-color: rgba(255, 255, 255, 0.6);
        /* Black w/opacity/see-through */
        color: #333333;
        border: 3px solid #f1f1f1;
        padding: 10px;
    }
</style>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @php
        $active = 'active';
        $no = 0;
        @endphp
        @foreach ($banner->data as $row )

        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $no }}" class="$active"></li>
        @php
        $active = '';
        $no++;
        @endphp
        @endforeach


    </ol>
    <div class="carousel-inner">
        @php
        $active = 'active';
        $no = 0;
        @endphp
        @foreach ($banner->data as $row )
        <div class="carousel-item {{ $active }}">
            <img class="d-block w-100" width="50%" src="{{ $row->link }}" alt="{{ $row->title }}">
            <div class="carousel-caption d-none d-md-block">
                <h5 class="avatar-box">{{ $row->title }}</h5>

            </div>
        </div>
        @php
        $active = '';
        $no++;
        @endphp
        @endforeach


    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>