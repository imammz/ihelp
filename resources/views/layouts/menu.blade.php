<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
    <div class="container-fluid">
        <!--begin::Header Menu-->
        <div id="kt_header_menu"
            class="header-menu header-menu-left header-menu-mobile header-menu-layout-default header-menu-root-arrow">
            <!--begin::Header Nav-->
            <ul class="menu-nav">


                {{--   <li class="menu-item menu-item-submenu menu-item-rel menu-item-open menu-item-here" data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:;" class="menu-link menu-toggle">
                       
                        <span class="menu-text">
                            <i class="fas fa-home"></i>
                            &nbsp;
                            Dashboard
                        </span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                        <ul class="menu-subnav">
                            <li class="menu-item menu-item-open menu-item-here" aria-haspopup="true">
                                <a href="index.html" class="menu-link">
                                    <span class="menu-text">Application</span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                            <li class="menu-item" aria-haspopup="true">
                                <a target="_blank" href="https://preview.keenthemes.com/metronic/demo8/builder.html" class="menu-link">
                                    <span class="menu-text">Layout Builder</span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>  --}}

                @foreach ($menus as $row)

                @php
                $active = '';
                $iconActive = '';
                @endphp

                @if(strtoupper($current) == strtoupper($row['menu']))
                @php
                $active = 'menu-item-here';
                $iconActive = 'text-info';
                @endphp
                @endif
                <li class="menu-item menu-item-submenu menu-item-rel menu-item-open {{ $active }}"
                    data-menu-toggle="click" aria-haspopup="true">

                    <a href="{{ url('/'.$row['link']) }}"
                        class="menu-link @if(count($row['submenu']) > 0 ) menu-toggle @endif">

                        <span class="menu-text">
                            <i class="{{ $iconActive }} {{ $row['icon'] }}"></i>
                            &nbsp;
                            {{ $row['menu'] }}
                        </span>
                        @if(count($row['submenu']) > 0 )
                        <i class="menu-arrow"></i>
                        @endif
                    </a>

                    @if(count($row['submenu']) > 0 )
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                        <ul class="menu-subnav">
                            @foreach ($row['submenu'] as $rowsub)

                            @if(url()->current() == url(''.$rowsub['link']))
                            @php
                            $activeSub = 'menu-item-here';
                            @endphp
                            @else
                            @php
                            $activeSub = '';
                            @endphp
                            @endif

                            <li class="menu-item menu-item-open {{ $activeSub }}" aria-haspopup="true">
                                <a href="{{ url('/'.$rowsub['link']) }}" class="menu-link">
                                    <span class="menu-text">
                                        <i class="fas {{ $rowsub['icon'] }}"></i>
                                        &nbsp;
                                        {{ $rowsub['menu'] }}

                                    </span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                            @endforeach


                        </ul>
                    </div>
                    @endif
                </li>

                @endforeach


            </ul>
            <!--end::Header Nav-->
        </div>
        <!--end::Header Menu-->
    </div>
</div>