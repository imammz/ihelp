<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@php
    
$userData = session('userData');

@endphp
<html lang="en">
	<!--begin::Head-->
	<head><base href="{{ url('/') }}/themes/v8/assets/">
		<meta charset="utf-8" />
		<title>General Cards | Keenthemes</title>
		<meta name="description" content="General card examples" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{ url('/') }}/themes/v8/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/themes/v8/assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/themes/v8/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" href="{{ url('/') }}/themes/v8/assets/media/logos/favicon.ico" />
        
        @yield('css')

	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed subheader-enabled page-loading">
    
    <div id="app"></div>
		
		<!--end::Demo Panel-->
		<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="{{ url('/') }}/themes/v8/assets/plugins/global/plugins.bundle.js"></script>
		<script src="{{ url('/') }}/themes/v8/assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
		<script src="{{ url('/') }}/themes/v8/assets/js/scripts.bundle.js"></script>


 
    <script src="{{ mix('js/app.js') }}"></script>

        @yield('js')

		<!--end::Global Theme Bundle-->
	</body>
	<!--end::Body-->
</html>