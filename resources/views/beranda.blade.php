@extends('layouts.v8')

@section('css')

<link href="{{ url('/') }}/themes/v8/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet"
    type="text/css" />

@endsection


@section('content')

<div class="row">
    <div class="col-lg-8">

        <div class="card card-custom gutter-b">

            <iframe src="{{ url('/banner') }}" style="width:100%; height: 420px; border: none;" scrolling="no"></iframe>


        </div>


    </div>


    <div class="col-lg-4">


        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0">
                <h3 class="card-title font-weight-bolder text-dark">Aturan</h3>

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-0" style="min-height: 325px">

                @foreach ($aturan->data as $row)


                <!--begin::Item-->
                <div class="d-flex align-items-center mb-9 bg-light-info rounded p-5">
                    <!--begin::Icon-->
                    <span class="svg-icon svg-icon-warning mr-5">
                        <span class="svg-icon svg-icon-lg">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Home/Library.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path
                                        d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z"
                                        fill="#000000"></path>
                                    <rect fill="#000000" opacity="0.3"
                                        transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519)"
                                        x="16.3255682" y="2.94551858" width="3" height="18" rx="1"></rect>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                    </span>
                    <!--end::Icon-->
                    <!--begin::Title-->
                    <div class="d-flex flex-column flex-grow-1 mr-2">
                        <h2 class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">
                            {{ $row->title }}
                        </h2>
                        <span class="text-muted font-weight-bold">{{ $row->catatan }}</span>
                    </div>
                    <!--end::Title-->
                    <!--begin::Lable-->
                    <span class="font-weight-bolder text-warning py-1 font-size-lg">
                        <a href="{{ $row->lampiran}}" target="_blank">
                            <i class="fas fa-download"></i>
                        </a>
                    </span>
                    <!--end::Lable-->
                </div>
                <!--end::Item-->

                @endforeach

            </div>
            <!--end::Body-->
        </div>



    </div>

</div>

<div class="row">
    <div class="col-lg-6">
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header align-items-center border-0 mt-4">
                <h3 class="card-title align-items-start flex-column">
                    <span class="font-weight-bolder text-dark">Rekap Komplain Job</span>
                </h3>

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-4">
                <!--begin::Timeline-->
                <div class="">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card card-custom bg-info card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">
                                        @if(isset($tenant_Open->data))
                                        {{ count($tenant_Open->data) }}
                                        @else
                                        0
                                        @endif
                                    </span>
                                    <span class="font-weight-bold text-white font-size-sm">Open</span>
                                </div>
                                <!--end::Body-->
                            </div>

                        </div>
                        <div class="col-lg-3">
                            <div class="card card-custom bg-danger card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">
                                        @if(isset($tenant_Pending->data))
                                        {{ count($tenant_Pending->data) }}
                                        @else
                                        0
                                        @endif
                                    </span>
                                    <span class="font-weight-bold text-white font-size-sm">Pending</span>
                                </div>
                                <!--end::Body-->
                            </div>


                        </div>
                        <div class="col-lg-3">
                            <div class="card card-custom bg-warning card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">
                                        @if(isset($tenant_Progress->data))
                                        {{ count($tenant_Progress->data) }}
                                        @else
                                        0
                                        @endif
                                    </span>
                                    <span class="font-weight-bold text-white font-size-sm">Progress</span>
                                </div>
                                <!--end::Body-->
                            </div>


                        </div>
                        <div class="col-lg-3">
                            <div class="card card-custom bg-success card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">
                                        @if(isset($tenant_Done->data))
                                        {{ count($tenant_Done->data) }}
                                        @else
                                        0
                                        @endif
                                    </span>
                                    <span class="font-weight-bold text-white font-size-sm">Finish</span>
                                </div>
                                <!--end::Body-->
                            </div>
                        </div>

                    </div>


                </div>
                <!--end::Timeline-->
            </div>
            <!--end: Card Body-->
        </div>

    </div>
    <div class="col-lg-6">
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header align-items-center border-0 mt-4">
                <h3 class="card-title align-items-start flex-column">
                    <span class="font-weight-bolder text-dark">Rekap Internal Job</span>
                </h3>

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-4">
                <!--begin::Timeline-->
                <div class="">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card card-custom bg-info card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">
                                        @if(isset($internal_Open->data))
                                        {{ count($internal_Open->data) }}
                                        @else
                                        0
                                        @endif
                                    </span>
                                    <span class="font-weight-bold text-white font-size-sm">Open</span>
                                </div>
                                <!--end::Body-->
                            </div>

                        </div>
                        <div class="col-lg-3">
                            <div class="card card-custom bg-danger card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">@if(isset($internal_Pending->data))
                                        {{ count($internal_Pending->data) }}
                                        @else
                                        0
                                        @endif</span>
                                    <span class="font-weight-bold text-white font-size-sm">Pending</span>
                                </div>
                                <!--end::Body-->
                            </div>


                        </div>
                        <div class="col-lg-3">
                            <div class="card card-custom bg-warning card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">@if(isset($internal_Progress->data))
                                        {{ count($internal_Progress->data) }}
                                        @else
                                        0
                                        @endif</span>
                                    <span class="font-weight-bold text-white font-size-sm">Progress</span>
                                </div>
                                <!--end::Body-->
                            </div>


                        </div>
                        <div class="col-lg-3">
                            <div class="card card-custom bg-success card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <span class="svg-icon svg-icon-2x svg-icon-white">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->

                                        <!--end::Svg Icon-->
                                    </span>
                                    <span
                                        class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block">@if(isset($internal_Done->data))
                                        {{ count($internal_Done->data) }}
                                        @else
                                        0
                                        @endif</span>
                                    <span class="font-weight-bold text-white font-size-sm">Finish</span>
                                </div>
                                <!--end::Body-->
                            </div>
                        </div>

                    </div>


                </div>
                <!--end::Timeline-->
            </div>
            <!--end: Card Body-->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header align-items-center border-0 mt-4">
                <h3 class="card-title align-items-start flex-column">
                    <span class="font-weight-bolder text-dark">Reminder</span>
                </h3>

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-4">
                <!--begin::Timeline-->
                <div class="timeline timeline-6 mt-3">

                    @foreach ($reminders->data as $row )


                    <!--begin::Item-->
                    <div class="timeline-item align-items-start">
                        <!--begin::Label-->
                        <div class="timeline-label font-weight-bolder text-dark-75 font-size-sm">
                            {{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->date))->diffForHumans()  }}
                        </div>
                        <!--end::Label-->
                        <!--begin::Badge-->
                        <div class="timeline-badge">
                            <i class="fa fa-genderless text-danger icon-xl"></i>
                        </div>
                        <!--end::Badge-->
                        <!--begin::Text-->
                        <div class="font-weight-mormal font-size-lg timeline-content text-dark pl-3">
                            <span>
                                {{ $row->title  }} </span>
                            <p class="text-muted">
                                ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->date))->format('d, M Y') }})
                                <br />
                                {{ $row->desc  }} </p>
                        </div>
                        <!--end::Text-->
                    </div>
                    <!--end::Item-->

                    @endforeach

                </div>
                <!--end::Timeline-->
            </div>
            <!--end: Card Body-->
        </div>
    </div>
    <div class="col-6">

        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 mt-5">
                <h3 class="card-title align-items-start flex-column text-dark">
                    <span class="font-weight-bolder text-dark">Newslatter</span>
                </h3>

            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body pt-2">
                @foreach ($newsletters->data as $row )


                <!--begin::Item-->
                <div class="d-flex align-items-center mb-10">
                    <!--begin::Symbol-->
                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                        <img src="{{ $row->media }}">
                    </div>
                    <!--end::Symbol-->
                    <!--begin::Section-->
                    <div class="d-flex flex-column flex-grow-1">
                        <!--begin::Title-->
                        <a href="#" data-toggle="modal" data-target="#exampleModalLong{{ $row->id }}"
                            class="text-dark font-weight-bolder font-size-lg text-hover-primary mb-1">
                            {{ $row->title }}
                        </a>
                        <span class="text-muted">
                            {{ \Carbon\Carbon::createFromTimeStamp(strtotime($row->date))->diffForHumans()  }}
                        </span>
                        <!--end::Title-->
                        <!--begin::Desc-->
                        <span class="text-dark-50 font-weight-normal font-size-sm">
                            {{ $row->desc }}
                            <!--begin::Desc-->
                    </div>
                    <!--end::Section-->
                </div>
                <!--end::Item-->

                <!-- Modal-->
                <div class="modal fade" id="exampleModalLong{{ $row->id }}" data-backdrop="static" tabindex="-1"
                    role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{ $row->title }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i aria-hidden="true" class="ki ki-close"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img width="100%" src="{{ $row->media }}">
                            </div>

                        </div>
                    </div>
                </div>

                @endforeach
            </div>
            <!--end::Body-->
        </div>

    </div>



</div>




@endsection

@section('js')
<script src="{{ url('/') }}/themes/v8/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js">
</script>


<script src="{{ url('/') }}/themes/v8/assets/js/pages/features/calendar/basic.js"></script>

@endsection